package com.marlabs.rss;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class JMSPublisher {
	public static void main(String[] args) throws JMSException {
		ConnectionFactory cf = new ActiveMQConnectionFactory();
		Connection conn = cf.createConnection();
		conn.start();
		Session s = conn.createSession(true, Session.AUTO_ACKNOWLEDGE);
		Destination d = s.createTopic("MyTopic2");
		MessageProducer mp = s.createProducer(d);
		TextMessage tm = s.createTextMessage("publis publis publis 11111");
		mp.send(tm);
		s.commit();

		mp.close();
		s.close();
		conn.close();

	}
}
