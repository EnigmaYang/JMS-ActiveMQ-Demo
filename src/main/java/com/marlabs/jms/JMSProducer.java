package com.marlabs.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;

public class JMSProducer {

	private static final String USERNAME = ActiveMQConnection.DEFAULT_USER;
	private static final String PASSWORD = ActiveMQConnection.DEFAULT_PASSWORD;
//	private static final String BROKEURL = ActiveMQConnection.DEFAULT_BROKER_URL;
	private static final int SENDNUM = 10;

	private static final Logger LOGGER = Logger.getLogger(JMSProducer.class.getName());

	public static void main(String[] args) throws JMSException {

		LOGGER.info("USERNAME: " + USERNAME);
		LOGGER.info("PASSWORD: " + PASSWORD);

		ConnectionFactory connectionFactory;
		Connection connection;
		Session session; // 发送消息的线程
		Destination destination; // 消息的目的地
		MessageProducer messageProducer; // 消息生产者

		connectionFactory = new ActiveMQConnectionFactory();
		// connection = connectionFactory.createConnection(USERNAME, PASSWORD);
		connection = connectionFactory.createConnection();
		connection.start();
		session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		destination = session.createQueue("FirstQueue1");
		messageProducer = session.createProducer(destination);
		sentMessage(session, messageProducer);
		session.commit();

		messageProducer.close();
		session.close();
		connection.close();

	}

	public static void sentMessage(Session session, MessageProducer messageProducer) throws JMSException {
		for (int i = 0; i < SENDNUM; i++) {
			TextMessage message = session.createTextMessage("ACtiveMQ sent message form sentMessage() " + i);
			System.out.println("message : " + message);
			messageProducer.send(message);
		}
	}

}
