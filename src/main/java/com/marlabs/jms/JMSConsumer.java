package com.marlabs.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.spring.ActiveMQConnectionFactory;

public class JMSConsumer {
	public static void main(String[] args) throws JMSException {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		Destination destination = session.createQueue("TEST.FOO");
		MessageConsumer messageConsumer = session.createConsumer(destination);
		for (int i = 0; i < 9; i++)
			consumeMessage(session, messageConsumer);
		session.commit();
		messageConsumer.close();
		session.close();
		connection.close();
	}

	private static void consumeMessage(Session session, MessageConsumer messageConsumer) throws JMSException {
		TextMessage message = (TextMessage) messageConsumer.receive();
		System.out.println(message.getText());
	}

}
