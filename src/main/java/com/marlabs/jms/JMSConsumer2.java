package com.marlabs.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.spring.ActiveMQConnectionFactory;

public class JMSConsumer2 {
	public static void main(String[] args) throws JMSException {
		ConnectionFactory cf = new ActiveMQConnectionFactory();
		Connection conn = cf.createConnection();
		conn.start();
		Session s = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
		Destination dest = s.createQueue("FirstQueue1");
		MessageConsumer mc = s.createConsumer(dest);
		mc.setMessageListener(new MessageListener() {
			public void onMessage(Message message) {
				try {
					System.out.println("Message is in MListener: " + ((TextMessage) message).getText());
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
